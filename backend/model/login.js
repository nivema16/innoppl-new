// contactModel.js
var mongoose = require('mongoose');
// Setup schema
var loginSchema = mongoose.Schema({
    name: String,
    pwd: String
});

// Export Contact model
var Login = module.exports = mongoose.model('logins', loginSchema);
module.exports.get = function (param,callback) {
    Login.find({name:param.body.name,password:param.body.password},(err,data)=>{
        callback(err,data);
    });
}