var mongoose = require("mongoose");
// Setup schema
var deptSchema = mongoose.Schema({
  name: String,
  id: String,
});

var Dept = (module.exports = mongoose.model("departments", deptSchema));
module.exports.get = function (callback) {
  Dept.find({}, (err, data) => {
    callback(err, data);
  });
};

module.exports.getNames = function (deptIds) {
  // Dept.find({ 'id': { $in: deptIds } },(err,data)=>{
  //     console.log(data)
  // })

  return Dept.find({ id: { $in: deptIds } });
};
