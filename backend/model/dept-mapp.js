var mongoose = require('mongoose');
// Setup schema
var deptMapSchema = mongoose.Schema({
    dept_id: String,
    emp_id: String
});

var Dept = module.exports = mongoose.model('departmentmappings', deptMapSchema);
module.exports.save = function (req,callback) {
    var insertList = [];

    req.body.depts.forEach(element => {
        let ip = {};
        ip.emp_id = req.body.empData.id;
        ip.dept_id = element.id;
        insertList.push(ip);
    });
    
    Dept.insertMany(insertList).then(function(){
        callback(null);
    }).catch(function(error){
        callback(error);    // Failure
    });
}

module.exports.empDetails = async function (employee,callback) {
    // Dept.find({ 'emp_id': { $in: employee } },(err,data)=>{
    //     callback(err,data)
    // });
    

        Dept.aggregate([
            
            { 
                $match:{'emp_id':{ $in: employee }} 
            }
            ,
            {
                $group:{_id:'$emp_id',emp_id:{ "$first": "$emp_id" },'dept':{$push:'$dept_id'}}
            }
        
        ]).then((data)=>{
            callback(null,data);
        });
    
}