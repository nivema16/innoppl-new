// api-routes.js
// Initialize express router
let router = require('express').Router();
// Set default API response
router.get('/', function (req, res) {
    res.json({
        status: 'API Its Working',
        message: 'Welcome !',
    });
});
// Import contact controller
var employeeList = require('./controller/employeelist');
var login = require('./controller/loginController');
var dept = require('./controller/deptController');
var pjct = require('./controller/pjctController');
// Contact routes
router.route('/employee-list')
     .get(employeeList.employeeList);
router.route('/login')
     .post(login.loginDetails);
router.route('/dept')
     .get(dept.deptDetails);
router.route('/dept-insert')
     .post(dept.deptMapping);
router.route('/pjct')
     .get(pjct.pjctDetails);
router.route('/pjct-insert')
     .post(pjct.pjctMapping);
router.route('/employee-details')
     .get(employeeList.employeeDetails);
//     .post(contactController.new);
// router.route('/contacts/:contact_id')
//     .get(contactController.view)
//     .patch(contactController.update)
//     .put(contactController.update)
//     .delete(contactController.delete);


// Export API routes
module.exports = router;