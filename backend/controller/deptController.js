var Dept = require('../model/dept');
var DeptMap = require('../model/dept-mapp');
exports.deptDetails = function(req, res) {
    Dept.get(function (err, dept) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Dept details retrieved successfully",
            data: dept
        });
    });

        
};

exports.deptMapping = function(req,res){
    DeptMap.save(req,function (err) {
        if (err) {
            res.json({
                status: "error",
                message: err,
            });
        }
        res.json({
            status: "success",
            message: "Dept details inserted successfully"
        });
    })
}