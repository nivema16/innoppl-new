var Employee = require("../model/employee");
var Dept = require("../model/dept-mapp");
var Department = require("../model/dept");
var Project = require("../model/pjct");
var ProjectMapping = require("../model/pjct-map");

exports.employeeList = function (req, res) {
  Employee.get(function (err, employee) {
    if (err) {
      res.json({
        status: "error",
        message: err,
      });
    }
    res.json({
      status: "success",
      message: "employee retrieved successfully",
      data: employee,
    });
  });
};

exports.employeeDetails = function (req, res) {
  Employee.get(async function (err, employee) {
    let result = [];
    if (err) {
      res.json({
        status: "error",
        message: err,
      });
    }
    let empIdList = [];
    employee.forEach((element) => {
      empIdList.push(element.id);
    });

    Dept.empDetails(empIdList, async function (err, dept) {
      if (err) {
        res.json({
          status: "error",
          message: err,
        });
      }

      for (let i = 0; i < dept.length; i++) {
        var resultObj = {
          emp_id: dept[i].emp_id,
        };

        let data = await Department.getNames(dept[i].dept);

        resultObj.deptData = data;
        result.push(resultObj);
      }

      getPjctDetails(empIdList, result, res);
    });
  });
};

function getPjctDetails(empIdList, result, res) {
  console.log("pjct check");
  return ProjectMapping.empDetails(empIdList, async function (err, pjct) {
    if (err) {
      res.json({
        status: "error",
        message: err,
      });
    }

    for (let i = 0; i < pjct.length; i++) {
      var resultObj = result.filter((project) => {
        return project.emp_id === pjct[i].emp_id;
      });

      let data = await Project.getNames(pjct[i].pjct);

      if (resultObj[0]) {
        resultObj[0].pjctData = data;
      } else {
        resultObj[0].pjctData = [];
      }
    }

    res.json({
      status: "success",
      message: "Department details retrieved successfully",
      data: result,
    });
  });
}
