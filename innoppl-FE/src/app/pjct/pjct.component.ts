import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { EmpListService } from '../list/emp-list.service';

import { PjctService } from '../services/pjct.service';

@Component({
  selector: 'app-pjct',
  templateUrl: './pjct.component.html',
  styleUrls: ['./pjct.component.scss']
})
export class PjctComponent implements OnInit {

  selectedPjct = [];
  pjctData = [];
  selectedEmp = {};
  empData = [];
  constructor(private pjctService:PjctService,private empListService:EmpListService) { }

  ngOnInit(): void {
    

    forkJoin([this.pjctService.getData(),this.empListService.getList()]).subscribe(result=>{
      if(result[0].data){
        this.pjctData = result[0].data.map((v:any)=>({
          id:v.id,
          name:v.name
        }))
      }

      if(result[1].data){
        this.empData = result[1].data;
      }
    })
  }

  mapData(){
    if(this.selectedPjct){
      let data = {
        empData : this.selectedEmp,
        pjcts : this.selectedPjct
      }
      this.pjctService.mapPjct(data).subscribe((data:any)=>{
        alert('Hurray! from pjct mapping')
      })
    }
    
  }

}
