import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PjctComponent } from './pjct.component';

describe('PjctComponent', () => {
  let component: PjctComponent;
  let fixture: ComponentFixture<PjctComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PjctComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PjctComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
