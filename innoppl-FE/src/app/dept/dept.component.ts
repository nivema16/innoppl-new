import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { EmpListService } from '../list/emp-list.service';
import { DeptService } from './dept.service';

@Component({
  selector: 'app-dept',
  templateUrl: './dept.component.html',
  styleUrls: ['./dept.component.scss']
})
export class DeptComponent implements OnInit {
  selected = [];
  deptData = [];
  selectedEmp = {};
  empData = [];
  constructor(private deptService:DeptService,private empListService:EmpListService) { }

  ngOnInit(): void {
    this.deptService.getDeptData().subscribe(data=>{
      
      
    })

    forkJoin([this.deptService.getDeptData(),this.empListService.getList()]).subscribe(result=>{
      if(result[0].data){
        this.deptData = result[0].data.map((v:any)=>({
          id:v.id,
          name:v.name
        }))
      }

      if(result[1].data){
        this.empData = result[1].data;
      }
    })
  }

  deptChange(){
    console.log(this.selected)
  }

  mapData(){
    if(this.selectedEmp){
      let data = {
        empData : this.selectedEmp,
        depts : this.selected
      }
      this.deptService.mapDept(data).subscribe((data)=>{
        alert('Hurray!')
      })
    }
    
  }

}
