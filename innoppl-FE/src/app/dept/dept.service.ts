import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DeptService {

  constructor(private http:HttpClient) { }

  public getDeptData(): Observable<any>{
    return this.http.get('http://localhost:8080/api/dept');
  }

  public mapDept(data:any){
    return this.http.post('http://localhost:8080/api/dept-insert',data,{
      headers: {
        'Content-Type': "application/json"
      }
    })
   
  }
}
