import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmpListService {

  constructor(private http: HttpClient) { }

  public getList(): Observable<any>{
    return this.http.get('http://localhost:8080/api/employee-list')
  }

  public getDetails(): Observable<any>{
    return this.http.get('http://localhost:8080/api/employee-details')
  }
}
