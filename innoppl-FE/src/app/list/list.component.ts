import { Component, OnInit } from '@angular/core';
import { forkJoin } from 'rxjs';
import { EmpListService } from './emp-list.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  constructor(private empListService:EmpListService) { }

  deptFlag = false;
  listFlag = true;
  empDetails = [];
  ngOnInit() {
    this.empListService.getList().subscribe((data)=>{
    },err=>{

    })
    forkJoin([this.empListService.getList(),this.empListService.getDetails()]).subscribe(result=>{
      this.empDetails = result[1].data
    })
  }

  chooseDept(flag: boolean){
    this.deptFlag = flag;
    this.listFlag = false;
  }

  chooseList(flag:boolean){
    this.listFlag = flag;
    this.deptFlag = !flag
  }
}
