import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  public getRoot(param: { name: string; password: string; }): Observable<any>{
    return this.http.post('http://localhost:8080/api/login', param, {
      headers: {
        'Content-Type': "application/json"
      }
    })
}
}
