import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PjctService {

  constructor(private http:HttpClient) { }

  public getData(): Observable<any>{
    return this.http.get('http://localhost:8080/api/pjct');
  }

  public mapPjct(data:any){
    return this.http.post('http://localhost:8080/api/pjct-insert',data,{
      headers: {
        'Content-Type': "application/json"
      }
    })
   
  }
}
