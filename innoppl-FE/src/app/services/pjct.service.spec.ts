import { TestBed } from '@angular/core/testing';

import { PjctService } from './pjct.service';

describe('PjctService', () => {
  let service: PjctService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PjctService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
